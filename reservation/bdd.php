<?php

$dbUser = 'admin';
$dbPass = 'adminadmin';
$dbHost = 'localhost';
$dbName = 'agenda';

try {
    $cxn = new PDO('mysql:host=' . $dbHost . ';dbname=' . $dbName, $dbUser, $dbPass);
    $cxn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
} 
catch (PDOException $e) {
    echo 'Error: ' . $e->getMessage();
}


?>